## 拖动
  ### 整个拖动过程分为两块： 1 被拖动元素的事件 2 拖动到目标元素的事件
1. 被拖动元素事件
    ```
    dragstart: 按下鼠标键并开始移动鼠标的时候，会在被拖拽的元素上触发dragstart事件

    drag: 触发dragstart事件后，随即会触发drag事件，而在元素被拖动期间会持续触发drag事件（每隔几百毫秒）

    dragend: 当拖动停止时(无论把元素放到了有效的放置目标，还是放到了无效的放置目标上)，都会发生dragend事件。
    ```
2. 当某个元素被拖动到一个有效的放置目标的时候
    ```
    dragenter: 只要有元素被拖动到放置目标上，就会触发dragenter事件

    dragover: 被拖动的元素在放置目标的范围内移动时，会连续触发dragover事件

    dragleave： 如果元素被拖出放置目标，dragover事件不再发生，但是会触发dragleave事件（类似于mouseout事件）

    drop： 如果元素被放到了放置目标中会触发drop事件而不是dragleave事件
    ```