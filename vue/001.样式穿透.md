###  针对scoped样式 需要穿透修改UI组件内部样式：


1. 通用样式穿透
    ```css
    ::v-deep 想要修改的类名{
        要修改的样式
    }
    ```

2. sass 和 less 样式穿透
    ```css
    外层类  /deep/  想要修改的类名 {
    　　要修改的样式
    }

    /* 例如 */
    .wrapper /deep/ .el-card__header {

        border-bottom: none
    }
    ```

3. sytlus 的样式穿透 使用：（>>>）
    ```css
    外层类 >>> 想要修改的类名 {
    　　要修改的样式

    }
    
    .wrapper >>> .el-card__header {

        border-bottom: none
    }
    ```